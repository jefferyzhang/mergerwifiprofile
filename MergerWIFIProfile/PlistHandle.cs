﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MergerWIFIProfile
{
    public enum plistType
    {
        Auto,
        Binary,
        Xml
    }
    public static class PlistDateConverter
    {
        public static long timeDifference = 978307200L;

        public static long GetAppleTime(long unixTime)
        {
            return unixTime - PlistDateConverter.timeDifference;
        }

        public static long GetUnixTime(long appleTime)
        {
            return appleTime + PlistDateConverter.timeDifference;
        }

        public static System.DateTime ConvertFromAppleTimeStamp(double timestamp)
        {
            System.DateTime dateTime = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            return dateTime.AddSeconds(timestamp);
        }

        public static double ConvertToAppleTimeStamp(System.DateTime date)
        {
            System.DateTime d = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            return System.Math.Floor((date - d).TotalSeconds);
        }

        public static System.Collections.Generic.Dictionary<object, object> ParsePlist(System.Collections.Generic.Dictionary<object, object> dict, string itemName)
        {
            foreach (object current in dict.Keys)
            {
                if (current.Equals(itemName))
                {
                    System.Collections.Generic.Dictionary<object, object> dictionary = new System.Collections.Generic.Dictionary<object, object>();
                    System.Collections.Generic.Dictionary<object, object> result;
                    if (current is string)
                    {
                        System.Collections.Generic.Dictionary<string, object> dictionary2 = dict[itemName] as System.Collections.Generic.Dictionary<string, object>;
                        foreach (string current2 in dictionary2.Keys)
                        {
                            dictionary.Add(current2, dictionary2[current2]);
                        }
                        result = dictionary;
                        return result;
                    }
                    result = (dict[itemName] as System.Collections.Generic.Dictionary<object, object>);
                    return result;
                }
                else if (dict[current] is System.Collections.Generic.Dictionary<object, object>)
                {
                    PlistDateConverter.ParsePlist(dict[current] as System.Collections.Generic.Dictionary<object, object>, itemName);
                }
            }
            return null;
        }

        public static System.Collections.Generic.Dictionary<object, object> ParsePlist(byte[] pListData)
        {
            object obj = PListReadWrite.readPlist(pListData);
            System.Collections.Generic.Dictionary<object, object> dictionary = obj as System.Collections.Generic.Dictionary<object, object>;
            if (dictionary == null)
            {
                dictionary = new System.Collections.Generic.Dictionary<object, object>();
                System.Collections.Generic.Dictionary<string, object> dictionary2 = obj as System.Collections.Generic.Dictionary<string, object>;
                foreach (string current in dictionary2.Keys)
                {
                    dictionary.Add(current, dictionary2[current]);
                }
            }
            return dictionary;
        }

        public static byte[] GetItemBytesFromPListBytes(System.Collections.Generic.Dictionary<object, object> dict, string itemName)
        {
            string s = dict[itemName] as string;
            return System.Text.Encoding.Default.GetBytes(s);
        }

        public static string GetItemStringFromPlistBytes(System.Collections.Generic.Dictionary<object, object> dict, string itemName)
        {
            return dict[itemName] as string;
        }
    }
    public static class PListReadWrite
    {
        private static System.Collections.Generic.List<int> offsetTable = new System.Collections.Generic.List<int>();

        private static System.Collections.Generic.List<byte> objectTable = new System.Collections.Generic.List<byte>();

        private static int refCount;

        private static int objRefSize;

        private static int offsetByteSize;

        private static long offsetTableOffset;

        public static bool InsertLineBreaks = false;

        public static object readPlist(string path)
        {
            object result;
            using (System.IO.FileStream fileStream = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                result = PListReadWrite.readPlist(fileStream, plistType.Auto);
            }
            return result;
        }

        public static object readPlistSource(string source)
        {
            return PListReadWrite.readPlist(System.Text.Encoding.UTF8.GetBytes(source));
        }

        public static object readPlist(byte[] data)
        {
            return PListReadWrite.readPlist(new System.IO.MemoryStream(data), plistType.Auto);
        }

        public static plistType getPlistType(System.IO.Stream stream)
        {
            byte[] array = new byte[8];
            stream.Read(array, 0, 8);
            if (System.BitConverter.ToInt64(array, 0) == 3472403351741427810L)
            {
                return plistType.Binary;
            }
            return plistType.Xml;
        }

        public static object readPlist(System.IO.Stream stream, plistType type = plistType.Auto)
        {
            if (type == plistType.Auto)
            {
                type = PListReadWrite.getPlistType(stream);
                stream.Seek(0L, System.IO.SeekOrigin.Begin);
            }
            if (type == plistType.Binary)
            {
                using (System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(stream))
                {
                    byte[] data = binaryReader.ReadBytes((int)binaryReader.BaseStream.Length);
                    return PListReadWrite.readBinary(data);
                }
            }
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.XmlResolver = null;
            xmlDocument.Load(stream);
            return PListReadWrite.readXml(xmlDocument);
        }

        public static void writeXml(object value, string path)
        {
            using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(path))
            {
                streamWriter.Write(PListReadWrite.writeXml(value));
            }
        }

        public static void writeXml(object value, System.IO.Stream stream)
        {
            using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(stream))
            {
                streamWriter.Write(PListReadWrite.writeXml(value));
            }
        }

        public static string writeXml(object value)
        {
            string @string;
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream, new XmlWriterSettings
                {
                    Encoding = new System.Text.UTF8Encoding(false),
                    ConformanceLevel = ConformanceLevel.Document,
                    Indent = true
                }))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteDocType("plist", "-//Apple Computer//DTD PLIST 1.0//EN", "http://www.apple.com/DTDs/PropertyList-1.0.dtd", null);
                    xmlWriter.WriteStartElement("plist");
                    xmlWriter.WriteAttributeString("version", "1.0");
                    PListReadWrite.compose(value, xmlWriter);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Flush();
                    xmlWriter.Close();
                    @string = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
            return @string;
        }

        public static void writeBinary(object value, string path)
        {
            using (System.IO.BinaryWriter binaryWriter = new System.IO.BinaryWriter(new System.IO.FileStream(path, System.IO.FileMode.Create)))
            {
                binaryWriter.Write(PListReadWrite.writeBinary(value));
            }
        }

        public static void writeBinary(object value, System.IO.Stream stream)
        {
            using (System.IO.BinaryWriter binaryWriter = new System.IO.BinaryWriter(stream))
            {
                binaryWriter.Write(PListReadWrite.writeBinary(value));
            }
        }

        public static byte[] writeBinary(object value)
        {
            PListReadWrite.offsetTable.Clear();
            PListReadWrite.objectTable.Clear();
            PListReadWrite.refCount = 0;
            PListReadWrite.objRefSize = 0;
            PListReadWrite.offsetByteSize = 0;
            PListReadWrite.offsetTableOffset = 0L;
            int num = PListReadWrite.countObject(value) - 1;
            PListReadWrite.refCount = num;
            PListReadWrite.objRefSize = PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(PListReadWrite.refCount)).Length;
            PListReadWrite.composeBinary(value);
            PListReadWrite.writeBinaryString("bplist00", false);
            PListReadWrite.offsetTableOffset = (long)PListReadWrite.objectTable.Count;
            PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count - 8);
            PListReadWrite.offsetByteSize = PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(PListReadWrite.offsetTable[PListReadWrite.offsetTable.Count - 1])).Length;
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
            PListReadWrite.offsetTable.Reverse();
            for (int i = 0; i < PListReadWrite.offsetTable.Count; i++)
            {
                PListReadWrite.offsetTable[i] = PListReadWrite.objectTable.Count - PListReadWrite.offsetTable[i];
                byte[] array = PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(PListReadWrite.offsetTable[i]), PListReadWrite.offsetByteSize);
                System.Array.Reverse(array);
                list.AddRange(array);
            }
            PListReadWrite.objectTable.AddRange(list);
            PListReadWrite.objectTable.AddRange(new byte[6]);
            PListReadWrite.objectTable.Add(System.Convert.ToByte(PListReadWrite.offsetByteSize));
            PListReadWrite.objectTable.Add(System.Convert.ToByte(PListReadWrite.objRefSize));
            byte[] bytes = System.BitConverter.GetBytes((long)num + 1L);
            System.Array.Reverse(bytes);
            PListReadWrite.objectTable.AddRange(bytes);
            PListReadWrite.objectTable.AddRange(System.BitConverter.GetBytes(0L));
            bytes = System.BitConverter.GetBytes(PListReadWrite.offsetTableOffset);
            System.Array.Reverse(bytes);
            PListReadWrite.objectTable.AddRange(bytes);
            return PListReadWrite.objectTable.ToArray();
        }

        private static object readXml(XmlDocument xml)
        {
            XmlNode node = xml.DocumentElement.ChildNodes[0];
            return (System.Collections.Generic.Dictionary<string, object>)PListReadWrite.parse(node);
        }

        private static object readBinary(byte[] data)
        {
            PListReadWrite.offsetTable.Clear();
            System.Collections.Generic.List<byte> offsetTableBytes = new System.Collections.Generic.List<byte>();
            PListReadWrite.objectTable.Clear();
            PListReadWrite.refCount = 0;
            PListReadWrite.objRefSize = 0;
            PListReadWrite.offsetByteSize = 0;
            PListReadWrite.offsetTableOffset = 0L;
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(data);
            System.Collections.Generic.List<byte> range = list.GetRange(list.Count - 32, 32);
            PListReadWrite.parseTrailer(range);
            PListReadWrite.objectTable = list.GetRange(0, (int)PListReadWrite.offsetTableOffset);
            offsetTableBytes = list.GetRange((int)PListReadWrite.offsetTableOffset, list.Count - (int)PListReadWrite.offsetTableOffset - 32);
            PListReadWrite.parseOffsetTable(offsetTableBytes);
            return PListReadWrite.parseBinary(0);
        }

        private static System.Collections.Generic.Dictionary<string, object> parseDictionary(XmlNode node)
        {
            XmlNodeList childNodes = node.ChildNodes;
            if (childNodes.Count % 2 != 0)
            {
                throw new System.DataMisalignedException("Dictionary elements must have an even number of child nodes");
            }
            System.Collections.Generic.Dictionary<string, object> dictionary = new System.Collections.Generic.Dictionary<string, object>();
            for (int i = 0; i < childNodes.Count; i += 2)
            {
                XmlNode xmlNode = childNodes[i];
                XmlNode node2 = childNodes[i + 1];
                if (xmlNode.Name != "key")
                {
                    throw new System.ApplicationException("expected a key node");
                }
                object obj = PListReadWrite.parse(node2);
                if (obj != null && !dictionary.ContainsKey(xmlNode.InnerText))
                {
                    dictionary.Add(xmlNode.InnerText, obj);
                }
            }
            return dictionary;
        }

        private static System.Collections.Generic.List<object> parseArray(XmlNode node)
        {
            System.Collections.Generic.List<object> list = new System.Collections.Generic.List<object>();
            foreach (XmlNode node2 in node.ChildNodes)
            {
                object obj = PListReadWrite.parse(node2);
                if (obj != null)
                {
                    list.Add(obj);
                }
            }
            return list;
        }

        private static void composeArray(System.Collections.Generic.List<object> value, XmlWriter writer)
        {
            writer.WriteStartElement("array");
            foreach (object current in value)
            {
                PListReadWrite.compose(current, writer);
            }
            writer.WriteEndElement();
        }

        private static object parse(XmlNode node)
        {
            string name;
            switch (name = node.Name)
            {
                case "dict":
                    return PListReadWrite.parseDictionary(node);
                case "array":
                    return PListReadWrite.parseArray(node);
                case "string":
                    return node.InnerText;
                case "integer":
                    {
                        long num2 = 0L;
                        try
                        {
                            num2 = System.Convert.ToInt64(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                        }
                        catch (System.OverflowException)
                        {
                            ulong value = System.Convert.ToUInt64(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                            byte[] bytes = System.BitConverter.GetBytes(value);
                            num2 = System.BitConverter.ToInt64(bytes, 0);
                        }
                        return num2;
                    }
                case "real":
                    return System.Convert.ToDouble(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                case "false":
                    return false;
                case "true":
                    return true;
                case "null":
                    return null;
                case "date":
                    return XmlConvert.ToDateTime(node.InnerText, XmlDateTimeSerializationMode.Utc);
                case "data":
                    return System.Convert.FromBase64String(node.InnerText);
            }
            throw new System.ApplicationException(string.Format("Plist Node `{0}' is not supported", node.Name));
        }

        private static void compose(object value, XmlWriter writer)
        {
            if (value == null || value is string)
            {
                writer.WriteElementString("string", value as string);
                return;
            }
            if (value is int)
            {
                writer.WriteElementString("integer", ((int)value).ToString(System.Globalization.NumberFormatInfo.InvariantInfo));
                return;
            }
            if (value is uint)
            {
                writer.WriteElementString("integer", ((uint)value).ToString(System.Globalization.NumberFormatInfo.InvariantInfo));
                return;
            }
            if (value is long)
            {
                writer.WriteElementString("integer", ((long)value).ToString(System.Globalization.NumberFormatInfo.InvariantInfo));
                return;
            }
            if (value is System.Collections.Generic.Dictionary<string, object> || value.GetType().ToString().StartsWith("System.Collections.Generic.Dictionary`2[System.String"))
            {
                System.Collections.Generic.Dictionary<string, object> dictionary = value as System.Collections.Generic.Dictionary<string, object>;
                if (dictionary == null)
                {
                    dictionary = new System.Collections.Generic.Dictionary<string, object>();
                    System.Collections.IDictionary dictionary2 = (System.Collections.IDictionary)value;
                    foreach (object current in dictionary2.Keys)
                    {
                        dictionary.Add(current.ToString(), dictionary2[current]);
                    }
                }
                PListReadWrite.writeDictionaryValues(dictionary, writer);
                return;
            }
            if (value is System.Collections.Generic.List<object>)
            {
                PListReadWrite.composeArray((System.Collections.Generic.List<object>)value, writer);
                return;
            }
            if (value is byte[])
            {
                if (!PListReadWrite.InsertLineBreaks)
                {
                    writer.WriteElementString("data", System.Convert.ToBase64String((byte[])value));
                    return;
                }
                string value2 = System.Convert.ToBase64String((byte[])value, System.Base64FormattingOptions.InsertLineBreaks);
                writer.WriteElementString("data", value2);
                return;
            }
            else
            {
                if (value is float || value is double)
                {
                    writer.WriteElementString("real", ((double)value).ToString(System.Globalization.NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is System.DateTime)
                {
                    System.DateTime value3 = (System.DateTime)value;
                    string text = "";
                    if (!PListReadWrite.InsertLineBreaks)
                    {
                        text = XmlConvert.ToString(value3, XmlDateTimeSerializationMode.Utc);
                    }
                    else
                    {
                        text = XmlConvert.ToString(value3, XmlDateTimeSerializationMode.Utc);
                        if (text.Contains("."))
                        {
                            int num = text.LastIndexOf(".");
                            try
                            {
                                text = text.Remove(num, text.Length - num) + "Z";
                            }
                            catch (System.Exception)
                            {
                            }
                        }
                    }
                    writer.WriteElementString("date", text);
                    return;
                }
                if (value is bool)
                {
                    writer.WriteElementString(value.ToString().ToLower(), "");
                    return;
                }
                throw new System.Exception(string.Format("Value type '{0}' is unhandled", value.GetType().ToString()));
            }
        }

        private static void writeDictionaryValues(System.Collections.Generic.Dictionary<string, object> dictionary, XmlWriter writer)
        {
            writer.WriteStartElement("dict");
            foreach (string current in dictionary.Keys)
            {
                object value = dictionary[current];
                writer.WriteElementString("key", current);
                PListReadWrite.compose(value, writer);
            }
            writer.WriteEndElement();
        }

        private static int countObject(object value)
        {
            int num = 0;
            string a;
            
            if ((a = value.GetType().ToString()) != null)
            {
                if (value is System.Collections.Generic.Dictionary<string, object>)//== "System.Collections.Generic.Dictionary`2[System.String,System.Object]")
                {
                    System.Collections.Generic.Dictionary<string, object> dictionary = (System.Collections.Generic.Dictionary<string, object>)value;
                    foreach (string current in dictionary.Keys)
                    {
                        num += PListReadWrite.countObject(dictionary[current]);
                    }
                    num += dictionary.Keys.Count;
                    num++;
                    return num;
                }
                if (value is System.Collections.Generic.List<object>)//a == "System.Collections.Generic.List`1[System.Object]")
                {
                    System.Collections.Generic.List<object> list = (System.Collections.Generic.List<object>)value;
                    foreach (object current2 in list)
                    {
                        num += PListReadWrite.countObject(current2);
                    }
                    num++;
                    return num;
                }
            }
            num++;
            return num;
        }

        private static byte[] writeBinaryDictionary(System.Collections.Generic.Dictionary<string, object> dictionary)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<byte> list2 = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<int> list3 = new System.Collections.Generic.List<int>();
            for (int i = dictionary.Count - 1; i >= 0; i--)
            {
                object[] array = new object[dictionary.Count];
                dictionary.Values.CopyTo(array, 0);
                PListReadWrite.composeBinary(array[i]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            for (int j = dictionary.Count - 1; j >= 0; j--)
            {
                string[] array2 = new string[dictionary.Count];
                dictionary.Keys.CopyTo(array2, 0);
                PListReadWrite.composeBinary(array2[j]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            if (dictionary.Count < 15)
            {
                list2.Add(System.Convert.ToByte((int)(208 | System.Convert.ToByte(dictionary.Count))));
            }
            else
            {
                list2.Add(223);
                list2.AddRange(PListReadWrite.writeBinaryInteger(dictionary.Count, false));
            }
            foreach (int current in list3)
            {
                byte[] array3 = PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(current), PListReadWrite.objRefSize);
                System.Array.Reverse(array3);
                list.InsertRange(0, array3);
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] composeBinaryArray(System.Collections.Generic.List<object> objects)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<byte> list2 = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<int> list3 = new System.Collections.Generic.List<int>();
            for (int i = objects.Count - 1; i >= 0; i--)
            {
                PListReadWrite.composeBinary(objects[i]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            if (objects.Count < 15)
            {
                list2.Add(System.Convert.ToByte((int)(160 | System.Convert.ToByte(objects.Count))));
            }
            else
            {
                list2.Add(175);
                list2.AddRange(PListReadWrite.writeBinaryInteger(objects.Count, false));
            }
            foreach (int current in list3)
            {
                byte[] array = PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(current), PListReadWrite.objRefSize);
                System.Array.Reverse(array);
                list.InsertRange(0, array);
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] composeBinary(object obj)
        {
            string key;
            switch (key = obj.GetType().ToString())
            {
                case "System.Collections.Generic.Dictionary`2[System.String,System.Object]":
                    return PListReadWrite.writeBinaryDictionary((System.Collections.Generic.Dictionary<string, object>)obj);
                case "System.Collections.Generic.List`1[System.Object]":
                    return PListReadWrite.composeBinaryArray((System.Collections.Generic.List<object>)obj);
                case "System.Byte[]":
                    return PListReadWrite.writeBinaryByteArray((byte[])obj);
                case "System.Double":
                    return PListReadWrite.writeBinaryDouble((double)obj);
                case "System.Int32":
                case "System.Int64":
                case "System.UInt32":
                    return PListReadWrite.writeBinaryInteger(obj, true);
                case "System.String":
                    {
                        int byteCount = System.Text.Encoding.UTF8.GetByteCount((string)obj);
                        int length = ((string)obj).Length;
                        byte[] result;
                        if (length == byteCount)
                        {
                            result = PListReadWrite.writeBinaryString((string)obj, true);
                        }
                        else
                        {
                            result = PListReadWrite.writeBinaryUniString((string)obj, true);
                        }
                        return result;
                    }
                case "System.DateTime":
                    return PListReadWrite.writeBinaryDate((System.DateTime)obj);
                case "System.Boolean":
                    return PListReadWrite.writeBinaryBool((bool)obj);
            }
            return new byte[0];
        }

        public static byte[] writeBinaryDate(System.DateTime obj)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(PlistDateConverter.ConvertToAppleTimeStamp(obj)), 8));
            list.Reverse();
            list.Insert(0, 51);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        public static byte[] writeBinaryBool(bool obj)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(new byte[]
			{
				obj ? (byte)9 : (byte)8
			});
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryInteger(object value, bool write)
        {
            System.Collections.Generic.List<byte> list;
            if (value.GetType().ToString().Equals("System.Int32"))
            {
                list = new System.Collections.Generic.List<byte>(System.BitConverter.GetBytes((long)((int)value)));
            }
            else if (value.GetType().ToString().Equals("System.UInt32"))
            {
                list = new System.Collections.Generic.List<byte>(System.BitConverter.GetBytes((long)((ulong)((uint)value))));
            }
            else
            {
                list = new System.Collections.Generic.List<byte>(System.BitConverter.GetBytes((long)value));
            }
            list = new System.Collections.Generic.List<byte>(PListReadWrite.RegulateNullBytes(list.ToArray()));
            while ((double)list.Count != System.Math.Pow(2.0, System.Math.Log((double)list.Count) / System.Math.Log(2.0)))
            {
                list.Add(0);
            }
            int value2 = 16 | (int)(System.Math.Log((double)list.Count) / System.Math.Log(2.0));
            list.Reverse();
            list.Insert(0, System.Convert.ToByte(value2));
            if (write)
            {
                PListReadWrite.objectTable.InsertRange(0, list);
            }
            return list.ToArray();
        }

        private static byte[] writeBinaryDouble(double value)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(value), 4));
            while ((double)list.Count != System.Math.Pow(2.0, System.Math.Log((double)list.Count) / System.Math.Log(2.0)))
            {
                list.Add(0);
            }
            int value2 = 32 | (int)(System.Math.Log((double)list.Count) / System.Math.Log(2.0));
            list.Reverse();
            list.Insert(0, System.Convert.ToByte(value2));
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryByteArray(byte[] value)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(value);
            System.Collections.Generic.List<byte> list2 = new System.Collections.Generic.List<byte>();
            if (value.Length < 15)
            {
                list2.Add(System.Convert.ToByte((int)(64 | System.Convert.ToByte(value.Length))));
            }
            else
            {
                list2.Add(79);
                list2.AddRange(PListReadWrite.writeBinaryInteger(list.Count, false));
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryString(string value, bool head)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<byte> list2 = new System.Collections.Generic.List<byte>();
            char[] array = value.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                char value2 = array[i];
                list.Add(System.Convert.ToByte(value2));
            }
            if (head)
            {
                if (value.Length < 15)
                {
                    list2.Add(System.Convert.ToByte((int)(80 | System.Convert.ToByte(value.Length))));
                }
                else
                {
                    list2.Add(95);
                    list2.AddRange(PListReadWrite.writeBinaryInteger(list.Count, false));
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryUniString(string value, bool head)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>();
            System.Collections.Generic.List<byte> list2 = new System.Collections.Generic.List<byte>();
            System.Text.UnicodeEncoding unicodeEncoding = new System.Text.UnicodeEncoding();
            int byteCount = unicodeEncoding.GetByteCount(value);
            byte[] array = new byte[byteCount];
            unicodeEncoding.GetBytes(value, 0, value.Length, array, 0);
            int num = 0;
            while (num < array.Length && num + 1 < array.Length)
            {
                byte b = array[num];
                array[num] = array[num + 1];
                array[num + 1] = b;
                num += 2;
            }
            byte[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                byte item = array2[i];
                list.Add(item);
            }
            if (head)
            {
                if (value.Length < 15)
                {
                    list2.Add(System.Convert.ToByte((int)(96 | System.Convert.ToByte(value.Length))));
                }
                else
                {
                    list2.Add(111);
                    list2.AddRange(PListReadWrite.writeBinaryInteger(value.Length, false));
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] RegulateNullBytes(byte[] value)
        {
            return PListReadWrite.RegulateNullBytes(value, 1);
        }

        private static byte[] RegulateNullBytes(byte[] value, int minBytes)
        {
            System.Array.Reverse(value);
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(value);
            int num = 0;
            while (num < list.Count && list[num] == 0 && list.Count > minBytes)
            {
                list.Remove(list[num]);
                num--;
                num++;
            }
            if (list.Count < minBytes)
            {
                int num2 = minBytes - list.Count;
                for (int i = 0; i < num2; i++)
                {
                    list.Insert(0, 0);
                }
            }
            value = list.ToArray();
            System.Array.Reverse(value);
            return value;
        }

        private static void parseTrailer(System.Collections.Generic.List<byte> trailer)
        {
            PListReadWrite.offsetByteSize = System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(trailer.GetRange(6, 1).ToArray(), 4), 0);
            PListReadWrite.objRefSize = System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(trailer.GetRange(7, 1).ToArray(), 4), 0);
            byte[] array = trailer.GetRange(12, 4).ToArray();
            System.Array.Reverse(array);
            PListReadWrite.refCount = System.BitConverter.ToInt32(array, 0);
            byte[] array2 = trailer.GetRange(24, 8).ToArray();
            System.Array.Reverse(array2);
            PListReadWrite.offsetTableOffset = System.BitConverter.ToInt64(array2, 0);
        }

        private static void parseOffsetTable(System.Collections.Generic.List<byte> offsetTableBytes)
        {
            for (int i = 0; i < offsetTableBytes.Count; i += PListReadWrite.offsetByteSize)
            {
                byte[] array = offsetTableBytes.GetRange(i, PListReadWrite.offsetByteSize).ToArray();
                System.Array.Reverse(array);
                PListReadWrite.offsetTable.Add(System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
        }

        private static object parseBinaryDictionary(int objRef)
        {
            System.Collections.Generic.Dictionary<string, object> dictionary = new System.Collections.Generic.Dictionary<string, object>();
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            byte arg_23_0 = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            int num;
            int count = PListReadWrite.getCount(PListReadWrite.offsetTable[objRef], out num);
            if (count < 15)
            {
                num = PListReadWrite.offsetTable[objRef] + 1;
            }
            else
            {
                num = PListReadWrite.offsetTable[objRef] + 2 + PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(count), 1).Length;
            }
            for (int i = num; i < num + count * 2 * PListReadWrite.objRefSize; i += PListReadWrite.objRefSize)
            {
                byte[] array = PListReadWrite.objectTable.GetRange(i, PListReadWrite.objRefSize).ToArray();
                System.Array.Reverse(array);
                list.Add(System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
            for (int j = 0; j < count; j++)
            {
                dictionary.Add((string)PListReadWrite.parseBinary(list[j]), PListReadWrite.parseBinary(list[j + count]));
            }
            return dictionary;
        }

        private static object parseBinaryArray(int objRef)
        {
            System.Collections.Generic.List<object> list = new System.Collections.Generic.List<object>();
            System.Collections.Generic.List<int> list2 = new System.Collections.Generic.List<int>();
            byte arg_23_0 = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            int num;
            int count = PListReadWrite.getCount(PListReadWrite.offsetTable[objRef], out num);
            if (count < 15)
            {
                num = PListReadWrite.offsetTable[objRef] + 1;
            }
            else
            {
                num = PListReadWrite.offsetTable[objRef] + 2 + PListReadWrite.RegulateNullBytes(System.BitConverter.GetBytes(count), 1).Length;
            }
            for (int i = num; i < num + count * PListReadWrite.objRefSize; i += PListReadWrite.objRefSize)
            {
                byte[] array = PListReadWrite.objectTable.GetRange(i, PListReadWrite.objRefSize).ToArray();
                System.Array.Reverse(array);
                list2.Add(System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
            for (int j = 0; j < count; j++)
            {
                list.Add(PListReadWrite.parseBinary(list2[j]));
            }
            return list;
        }

        private static int getCount(int bytePosition, out int newBytePosition)
        {
            byte b = PListReadWrite.objectTable[bytePosition];
            byte b2 = System.Convert.ToByte((int)(b & 15));
            int result;
            if (b2 < 15)
            {
                result = (int)b2;
                newBytePosition = bytePosition + 1;
            }
            else
            {
                result = (int)PListReadWrite.parseBinaryInt(bytePosition + 1, out newBytePosition);
            }
            return result;
        }

        private static object parseBinary(int objRef)
        {
            byte b = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            int num = (int)(b & 240);
            if (num <= 48)
            {
                if (num <= 16)
                {
                    if (num != 0)
                    {
                        if (num == 16)
                        {
                            return PListReadWrite.parseBinaryInt64(PListReadWrite.offsetTable[objRef]);
                        }
                    }
                    else
                    {
                        if (PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]] != 0)
                        {
                            return PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]] == 9;
                        }
                        return null;
                    }
                }
                else
                {
                    if (num == 32)
                    {
                        return PListReadWrite.parseBinaryReal(PListReadWrite.offsetTable[objRef]);
                    }
                    if (num == 48)
                    {
                        return PListReadWrite.parseBinaryDate(PListReadWrite.offsetTable[objRef]);
                    }
                }
            }
            else if (num <= 80)
            {
                if (num == 64)
                {
                    return PListReadWrite.parseBinaryByteArray(PListReadWrite.offsetTable[objRef]);
                }
                if (num == 80)
                {
                    return PListReadWrite.parseBinaryAsciiString(PListReadWrite.offsetTable[objRef]);
                }
            }
            else
            {
                if (num == 96)
                {
                    return PListReadWrite.parseBinaryUnicodeString(PListReadWrite.offsetTable[objRef]);
                }
                if (num == 160)
                {
                    return PListReadWrite.parseBinaryArray(objRef);
                }
                if (num == 208)
                {
                    return PListReadWrite.parseBinaryDictionary(objRef);
                }
            }
            throw new System.Exception("This type is not supported");
        }

        public static object parseBinaryDate(int headerPosition)
        {
            byte[] array = PListReadWrite.objectTable.GetRange(headerPosition + 1, 8).ToArray();
            System.Array.Reverse(array);
            double timestamp = System.BitConverter.ToDouble(array, 0);
            System.DateTime dateTime = PlistDateConverter.ConvertFromAppleTimeStamp(timestamp);
            return dateTime;
        }

        private static object parseBinaryInt64(int headerPosition)
        {
            int num;
            return PListReadWrite.parseBinaryInt64(headerPosition, out num);
        }

        private static object parseBinaryInt(int headerPosition, out int newHeaderPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int num = (int)System.Math.Pow(2.0, (double)(b & 15));
            byte[] array = PListReadWrite.objectTable.GetRange(headerPosition + 1, num).ToArray();
            System.Array.Reverse(array);
            newHeaderPosition = headerPosition + num + 1;
            return System.BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0);
        }

        private static object parseBinaryInt64(int headerPosition, out int newHeaderPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int num = (int)System.Math.Pow(2.0, (double)(b & 15));
            byte[] array = PListReadWrite.objectTable.GetRange(headerPosition + 1, num).ToArray();
            System.Array.Reverse(array);
            newHeaderPosition = headerPosition + num + 1;
            return System.BitConverter.ToInt64(PListReadWrite.RegulateNullBytes(array, 8), 0);
        }

        private static object parseBinaryReal(int headerPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int count = (int)System.Math.Pow(2.0, (double)(b & 15));
            byte[] array = PListReadWrite.objectTable.GetRange(headerPosition + 1, count).ToArray();
            System.Array.Reverse(array);
            return System.BitConverter.ToDouble(PListReadWrite.RegulateNullBytes(array, 8), 0);
        }

        private static object parseBinaryAsciiString(int headerPosition)
        {
            int index;
            int count = PListReadWrite.getCount(headerPosition, out index);
            System.Collections.Generic.List<byte> range = PListReadWrite.objectTable.GetRange(index, count);
            if (range.Count <= 0)
            {
                return string.Empty;
            }
            return System.Text.Encoding.ASCII.GetString(range.ToArray());
        }

        private static object parseBinaryUnicodeString(int headerPosition)
        {
            int num2;
            int num = PListReadWrite.getCount(headerPosition, out num2);
            num *= 2;
            byte[] array = new byte[num];
            for (int i = 0; i < num; i += 2)
            {
                byte b = PListReadWrite.objectTable.GetRange(num2 + i, 1)[0];
                byte b2 = PListReadWrite.objectTable.GetRange(num2 + i + 1, 1)[0];
                if (System.BitConverter.IsLittleEndian)
                {
                    array[i] = b2;
                    array[i + 1] = b;
                }
                else
                {
                    array[i] = b;
                    array[i + 1] = b2;
                }
            }
            return System.Text.Encoding.Unicode.GetString(array);
        }

        private static object parseBinaryByteArray(int headerPosition)
        {
            int index;
            int count = PListReadWrite.getCount(headerPosition, out index);
            return PListReadWrite.objectTable.GetRange(index, count).ToArray();
        }
    }
}
