﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Xml;

namespace MergerWIFIProfile
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                System.Configuration.Install.InstallContext _args = new System.Configuration.Install.InstallContext(null, args);
                logIt("main function ++");
                if (_args.Parameters.ContainsKey("folder"))
                {
                    string folder = _args.Parameters["folder"];
                    ArrayList filList = GetzipFile(folder);
                    string targetFile = Path.Combine(Environment.GetEnvironmentVariable("APSTHOME"), "mergerWifiProfile.zip");
                    if (filList != null && filList.Count > 0)
                    {
                        if (filList.Count == 1)
                        {
                            string filezip = (string)filList[0];
                            
                            File.Copy(filezip, targetFile, true);
                            logIt(string.Format("Copy {0} to {1} successfully", filezip, targetFile));
                            logIt("main function --");
                            Environment.Exit(0);
                        }
                        else if (filList.Count > 1)
                        {
                            string date = DateTime.Now.ToString("yyyyMMddHHmm");
                            string zipTmp = Path.Combine(Path.GetTempPath(),date);                          
                            string tarGetzipFold = Path.Combine(zipTmp, "profileTmp");
                            if (!Directory.Exists(tarGetzipFold))
                            {
                                Directory.CreateDirectory(tarGetzipFold);
                            }

                            int i = 1;
                            foreach (string file in filList)
                            {
                                string filename = Path.GetFileNameWithoutExtension(file);
                                string targetTmpfolder = Path.Combine(zipTmp, filename);
                                if (!Directory.Exists(targetTmpfolder))
                                {
                                    Directory.CreateDirectory(targetTmpfolder);

                                }
                                unzip(file, targetTmpfolder);
                                //merger android profile
                                string androidprofile = Path.Combine(targetTmpfolder, "androidautoOpenWifi.xml");
                                string Targetandroidprofile = Path.Combine(tarGetzipFold, string.Format("androidautoOpenWifi{0}.xml", i));
                                File.Copy(androidprofile, Targetandroidprofile, true);
                                i++;

                                //merger iOS profile
                                string iosprofile = Path.Combine(targetTmpfolder, "iOSautoOpenWifi.mobileconfig");
                                string Targetiosprofile = Path.Combine(tarGetzipFold, "iOSautoOpenWifi.mobileconfig");
                                if (!File.Exists(Targetiosprofile))
                                {
                                    File.Copy(iosprofile, Targetiosprofile);
                                }
                                else
                                {
                                    //XmlDocument doc = new XmlDocument();
                                    //doc.Load(Targetiosprofile);
                                    //XmlDocument sourceDoc = new XmlDocument();
                                    //sourceDoc.Load(iosprofile);
                                    //XmlNode ArrNode = doc.SelectSingleNode("plist/dict/array");
                                    //XmlNode DicNode = sourceDoc.SelectSingleNode("plist/dict/array/dict");
                                    //XmlNode newNode = doc.CreateNode(XmlNodeType.Element, "dict", null);
                                    //newNode.InnerXml = DicNode.InnerXml;
                                    //ArrNode.AppendChild(newNode);
                                    //doc.Save(Targetiosprofile);
                                    System.Collections.Generic.Dictionary<string, object> plistdoc = (System.Collections.Generic.Dictionary<string, object>)PListReadWrite.readPlist(Targetiosprofile);
                                    System.Collections.Generic.Dictionary<string, object> plistSource = (System.Collections.Generic.Dictionary<string, object>)PListReadWrite.readPlist(iosprofile);
                                    if(plistdoc.ContainsKey("PayloadContent"))
                                    {
                                        System.Collections.Generic.List<object> list = (System.Collections.Generic.List<object>)plistdoc["PayloadContent"];
                                        if (plistSource.ContainsKey("PayloadContent"))
                                        {
                                            System.Collections.Generic.List<object> listSource = (System.Collections.Generic.List<object>)plistSource["PayloadContent"];
                                            list.AddRange(listSource);
                                        }
                                    }

                                    PListReadWrite.writeXml(plistdoc, Targetiosprofile);
                                }
                                
                                
                            }
                            //zip to one zipfile
                            string zipPath = zip(tarGetzipFold);
                            if (!string.IsNullOrEmpty(zipPath))
                            {
                                File.Copy(zipPath, targetFile, true);
                                logIt(string.Format("Copy {0} to {1} successfully", zipPath, targetFile));
                                try
                                {
                                    Directory.Delete(zipTmp, true);
                                }
                                catch (System.Exception ex)
                                {

                                }
                                logIt("main function --");
                                Environment.Exit(0);
                            }
                            else
                            {
                                logIt(string.Format("{0} is not exist"));
                                Environment.Exit(-1);
                            }
                           
                           
                        }
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Environment.Exit(-1);
                }
            }
            catch (System.Exception ex)
            {
                logIt("main function exception " + ex.ToString());
                Environment.Exit(-1);
            }

        }
        static ArrayList GetzipFile(string filePath)
        {
            ArrayList zipList = new ArrayList();
            try
            {
                string[] files = Directory.GetFiles(filePath);

                foreach (string file in files)
                {
                    string fileExt = Path.GetExtension(file);
                    if (string.Compare(fileExt, ".zip", true) == 0)
                    {
                        zipList.Add(file);
                    }
                }
            }
            catch (System.Exception ex)
            {

            }
            return zipList;


        }

        static string zip(string tarGetzipFold)
        {
            string  strzip = string.Empty;

            string zip_exe = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), "7z.exe");
            if (System.IO.File.Exists(zip_exe))
            {
                string zip_file = System.IO.Path.Combine(tarGetzipFold, "mergerWifiProfile.zip");
                if (System.IO.File.Exists(zip_file))
                {
                    System.IO.File.Delete(zip_file);
                }

                System.Diagnostics.ProcessStartInfo startInfo_zip = new System.Diagnostics.ProcessStartInfo();
                startInfo_zip.FileName = zip_exe;
                startInfo_zip.Arguments = string.Format("a -r -tzip \"{0}\" \"{1}\\*\"", zip_file, tarGetzipFold);
                startInfo_zip.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo_zip.CreateNoWindow = true;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(startInfo_zip);
                if (p != null)
                {
                    p.WaitForExit();
                    if (System.IO.File.Exists(zip_file))
                    {
                        strzip = zip_file;
                    }
                }


            }
            return strzip;
        }
        static int unzip(string zipfile, string targetDir)
        {
            int ret = 0;
            try
            {
                logIt("unzip file ++ " + zipfile);
                string zipexe = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), "7z.exe");
                if (System.IO.File.Exists(zipexe))
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = zipexe;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.Arguments = string.Format("x \"{0}\" -o\"{1}\" -aoa -r", zipfile, targetDir);
                    p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                    while (!p.StandardOutput.EndOfStream)
                    {
                        string line = p.StandardOutput.ReadLine();
                        logIt(line);
                    }
                    p.WaitForExit();
                    ret = p.ExitCode;
                }
                else
                {
                    logIt("missing 7z.exe");
                    ret = -1;
                }


            }
            catch (System.Exception ex)
            {
                logIt(string.Format("unzip {0} exception {1}", zipfile, ex.ToString()));
                ret = -1;
            }
            logIt(string.Format("unzip file -- return code={0} \n", ret));
            return ret;
        }
        static public void logIt(string s)
        {
            try
            {
                string ss = string.Format("[MergerWIFIProfile]: {0}", s);
                System.Diagnostics.Trace.WriteLine(ss);
            }
            catch (System.Exception)
            {

            }

        }
    }
}
